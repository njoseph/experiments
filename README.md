# Experiments

This repository contains my personal experiments and Proof of Concept programs written to learn some new language, framework, algorithm or technique.

Nothing in this repository is meant to be used in production. Expect it to be buggy and incomplete in implementation.

All files in this repository are licensed under GPLv3 or later.
