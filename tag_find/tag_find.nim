import os
import strutils

let currentDir = splitPath(currentSourcePath).head
let inputDir = paramStr(1)

let searchDir = if inputDir.len != 0 : inputDir else: currentDir

let searchTerm = "#+"

for file_name in walkDirRec(searchDir):
  let contents = readFile(file_name)
  let lines =  contents.split('\n')
  for line in lines:
    if line.startsWith(searchTerm):
      echo file_name
