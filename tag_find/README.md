# Tag Find

This badly-named experiment is to try out implementing a trivial program in multiple languages.

tag_find takes a tag and a directory path as arguments and searches the directory for all files tagged with that tag. Originally meant to be used to search emacs org files by tag and integrate into emacs with an elisp function.

Languages used:

* [Rust](https://rust-lang.org)
* [Go](https://golang.org)
* [Joker](https://joker-lang.org)
* [Racket](https://racket-lang.org)
* [Nim](https://nim-lang.org)

## Binary size comparison

Among the compiled languages tested, Nim produces the smallest binaries and Go
the largest. Rust is in between.

| Language | Binary size |
| -------- | -------- |
| Go       | 4100 KB  |
| Rust     | 447 KB   |
| Nim      | 123 KB   |


## Disclaimer
The implementations are written with hardly 3 hours of experience with each language and will perform poorly. Use at your own risk if you want to.
