package main

/*
Using a tag to test this file itself
#+tag
*/

import (
	"bufio"
	"fmt"
	"io/ioutil"
	cobra "github.com/spf13/cobra"
	"log"
	"os"
	"strings"
)

func main() {
	command := &cobra.Command{
		Use:          "tag-find",
		Short:        "Finds the files tagged with the requested tag",
		SilenceUsage: true,
		RunE: func(cmd *cobra.Command, args []string) error {
			tag := args[1]
			var dirName string
			if len(args) == 2 {
				dirName = "."
			} else {
				dirName = args[2]
			}
			cmd.Println("Trying to search for tag", tag, "in directory:", dirName)
			searchTag(tag, dirName)
			return nil
		},
	}

	if err := command.Execute(); err != nil {
		os.Exit(1)
	}

}

func searchTag(tag string, dirName string) {
	var containedFiles []string
	files, err := ioutil.ReadDir(dirName)
	if err != nil {
		log.Fatal(err)
	}

	for _, fileInfo := range files {
		fileName := fileInfo.Name()
		file, err := os.Open(fileName)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		startSequenceEnv := os.Getenv("TAG_FIND_START_SEQUENCE")
		var startSequence string

		if len(startSequenceEnv) != 0 {
			startSequence = startSequenceEnv
		} else {
			startSequence = "#"
		}

		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			if strings.HasPrefix(scanner.Text(), startSequence) && strings.Contains(scanner.Text(), tag) {
				containedFiles = append(containedFiles, fileName)
				break
			}
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}

	}

	if len(containedFiles) != 0 {
		for _, fileName := range containedFiles {
			fmt.Println(fileName)
		}
	}
}
