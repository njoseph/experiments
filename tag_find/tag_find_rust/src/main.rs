extern crate tag_find;

use std::env;
use std::process;

use tag_find::Config;

/// Searches all files in a directory for a tag name
fn main() {
    let args: Vec<String> = env::args().collect();

    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = tag_find::run(config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}


