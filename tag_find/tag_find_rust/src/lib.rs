use std::env;
use std::error;
use std::fs;
use std::io::{self, BufReader};
use std::io::prelude::*;


pub fn run(config: Config) -> Result<(), Box<dyn error::Error>> {

    let paths = fs::read_dir(config.dir_path).unwrap();

    for path in paths {
        let path_str = &path.unwrap().path().display().to_string();
        let md = std::fs::metadata(path_str).unwrap();
        if ! md.is_dir() {
            let result = search(&config.tag, path_str);
            if result.unwrap() {
                println!("{}", path_str);
            }
        }
    }

    Ok(())
}

pub struct Config {
    tag: String,
    dir_path: String,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {

        if args.len() < 3 {
            return Err("Not enough arguments");
        }

        let tag = args[1].clone();
        let dir_path = args[2].clone();

        Ok(Config {tag , dir_path})
    }
}


fn search<'a>(query: &str, path: &'a str) -> io::Result<bool> {
    let mut result = false;

    let f = fs::File::open(path)?;
    let f = BufReader::new(f);

    let start_sequence = &env::var("TAG_FIND_START_SEQUENCE").unwrap_or("#".to_string());

    for line in f.lines() {
        let line_str = line.unwrap();
        if line_str.starts_with(start_sequence) && line_str.contains(query) {
            result = true;
            break;
        }
    }

    Ok(result)
}
